// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "MyBTTask_FindRandomPatrol.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AITEST_MyBTTask_FindRandomPatrol_generated_h
#error "MyBTTask_FindRandomPatrol.generated.h already included, missing '#pragma once' in MyBTTask_FindRandomPatrol.h"
#endif
#define AITEST_MyBTTask_FindRandomPatrol_generated_h

#define FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_SPARSE_DATA
#define FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_ACCESSORS
#define FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyBTTask_FindRandomPatrol(); \
	friend struct Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics; \
public: \
	DECLARE_CLASS(UMyBTTask_FindRandomPatrol, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AITest"), NO_API) \
	DECLARE_SERIALIZER(UMyBTTask_FindRandomPatrol)


#define FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyBTTask_FindRandomPatrol(UMyBTTask_FindRandomPatrol&&); \
	NO_API UMyBTTask_FindRandomPatrol(const UMyBTTask_FindRandomPatrol&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyBTTask_FindRandomPatrol); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyBTTask_FindRandomPatrol); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyBTTask_FindRandomPatrol) \
	NO_API virtual ~UMyBTTask_FindRandomPatrol();


#define FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_12_PROLOG
#define FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_SPARSE_DATA \
	FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_ACCESSORS \
	FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_INCLASS_NO_PURE_DECLS \
	FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AITEST_API UClass* StaticClass<class UMyBTTask_FindRandomPatrol>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
