// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AITest/Public/MyBTTask_FindRandomPatrol.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyBTTask_FindRandomPatrol() {}
// Cross Module References
	AIMODULE_API UClass* Z_Construct_UClass_UBTTask_BlackboardBase();
	AITEST_API UClass* Z_Construct_UClass_UMyBTTask_FindRandomPatrol();
	AITEST_API UClass* Z_Construct_UClass_UMyBTTask_FindRandomPatrol_NoRegister();
	UPackage* Z_Construct_UPackage__Script_AITest();
// End Cross Module References
	void UMyBTTask_FindRandomPatrol::StaticRegisterNativesUMyBTTask_FindRandomPatrol()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UMyBTTask_FindRandomPatrol);
	UClass* Z_Construct_UClass_UMyBTTask_FindRandomPatrol_NoRegister()
	{
		return UMyBTTask_FindRandomPatrol::StaticClass();
	}
	struct Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SearchRadius_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_SearchRadius;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBTTask_BlackboardBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AITest,
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::DependentSingletons) < 16);
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "/**\n * \n */" },
#endif
		{ "IncludePath", "MyBTTask_FindRandomPatrol.h" },
		{ "ModuleRelativePath", "Public/MyBTTask_FindRandomPatrol.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::NewProp_SearchRadius_MetaData[] = {
		{ "Category", "AI" },
		{ "ModuleRelativePath", "Public/MyBTTask_FindRandomPatrol.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::NewProp_SearchRadius = { "SearchRadius", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, nullptr, nullptr, 1, STRUCT_OFFSET(UMyBTTask_FindRandomPatrol, SearchRadius), METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::NewProp_SearchRadius_MetaData), Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::NewProp_SearchRadius_MetaData) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::NewProp_SearchRadius,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMyBTTask_FindRandomPatrol>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::ClassParams = {
		&UMyBTTask_FindRandomPatrol::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::Class_MetaDataParams), Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::Class_MetaDataParams)
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::PropPointers) < 2048);
	UClass* Z_Construct_UClass_UMyBTTask_FindRandomPatrol()
	{
		if (!Z_Registration_Info_UClass_UMyBTTask_FindRandomPatrol.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UMyBTTask_FindRandomPatrol.OuterSingleton, Z_Construct_UClass_UMyBTTask_FindRandomPatrol_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UMyBTTask_FindRandomPatrol.OuterSingleton;
	}
	template<> AITEST_API UClass* StaticClass<UMyBTTask_FindRandomPatrol>()
	{
		return UMyBTTask_FindRandomPatrol::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMyBTTask_FindRandomPatrol);
	UMyBTTask_FindRandomPatrol::~UMyBTTask_FindRandomPatrol() {}
	struct Z_CompiledInDeferFile_FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UMyBTTask_FindRandomPatrol, UMyBTTask_FindRandomPatrol::StaticClass, TEXT("UMyBTTask_FindRandomPatrol"), &Z_Registration_Info_UClass_UMyBTTask_FindRandomPatrol, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UMyBTTask_FindRandomPatrol), 2862954602U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_2337201328(TEXT("/Script/AITest"),
		Z_CompiledInDeferFile_FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Unreal_AITest_AITest_Source_AITest_Public_MyBTTask_FindRandomPatrol_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
