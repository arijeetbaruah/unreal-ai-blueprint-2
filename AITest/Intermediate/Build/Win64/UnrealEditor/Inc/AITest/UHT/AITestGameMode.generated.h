// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "AITestGameMode.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AITEST_AITestGameMode_generated_h
#error "AITestGameMode.generated.h already included, missing '#pragma once' in AITestGameMode.h"
#endif
#define AITEST_AITestGameMode_generated_h

#define FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_SPARSE_DATA
#define FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_ACCESSORS
#define FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAITestGameMode(); \
	friend struct Z_Construct_UClass_AAITestGameMode_Statics; \
public: \
	DECLARE_CLASS(AAITestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/AITest"), AITEST_API) \
	DECLARE_SERIALIZER(AAITestGameMode)


#define FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AITEST_API AAITestGameMode(AAITestGameMode&&); \
	AITEST_API AAITestGameMode(const AAITestGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(AITEST_API, AAITestGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAITestGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAITestGameMode) \
	AITEST_API virtual ~AAITestGameMode();


#define FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_9_PROLOG
#define FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_SPARSE_DATA \
	FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_ACCESSORS \
	FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_INCLASS_NO_PURE_DECLS \
	FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AITEST_API UClass* StaticClass<class AAITestGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
