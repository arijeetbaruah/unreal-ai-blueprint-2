// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAITest_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_AITest;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_AITest()
	{
		if (!Z_Registration_Info_UPackage__Script_AITest.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/AITest",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0x0E550993,
				0x3FE34C47,
				METADATA_PARAMS(0, nullptr)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_AITest.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_AITest.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_AITest(Z_Construct_UPackage__Script_AITest, TEXT("/Script/AITest"), Z_Registration_Info_UPackage__Script_AITest, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x0E550993, 0x3FE34C47));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
