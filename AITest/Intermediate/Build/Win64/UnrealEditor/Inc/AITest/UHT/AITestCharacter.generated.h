// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "AITestCharacter.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AITEST_AITestCharacter_generated_h
#error "AITestCharacter.generated.h already included, missing '#pragma once' in AITestCharacter.h"
#endif
#define AITEST_AITestCharacter_generated_h

#define FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_SPARSE_DATA
#define FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_ACCESSORS
#define FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAITestCharacter(); \
	friend struct Z_Construct_UClass_AAITestCharacter_Statics; \
public: \
	DECLARE_CLASS(AAITestCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AITest"), NO_API) \
	DECLARE_SERIALIZER(AAITestCharacter)


#define FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAITestCharacter(AAITestCharacter&&); \
	NO_API AAITestCharacter(const AAITestCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAITestCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAITestCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAITestCharacter) \
	NO_API virtual ~AAITestCharacter();


#define FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_18_PROLOG
#define FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_SPARSE_DATA \
	FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_ACCESSORS \
	FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_INCLASS_NO_PURE_DECLS \
	FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AITEST_API UClass* StaticClass<class AAITestCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Unreal_AITest_AITest_Source_AITest_AITestCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
