// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AITest/AITestGameMode.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAITestGameMode() {}
// Cross Module References
	AITEST_API UClass* Z_Construct_UClass_AAITestGameMode();
	AITEST_API UClass* Z_Construct_UClass_AAITestGameMode_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_AITest();
// End Cross Module References
	void AAITestGameMode::StaticRegisterNativesAAITestGameMode()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AAITestGameMode);
	UClass* Z_Construct_UClass_AAITestGameMode_NoRegister()
	{
		return AAITestGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AAITestGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAITestGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AITest,
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_AAITestGameMode_Statics::DependentSingletons) < 16);
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAITestGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "AITestGameMode.h" },
		{ "ModuleRelativePath", "AITestGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAITestGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAITestGameMode>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AAITestGameMode_Statics::ClassParams = {
		&AAITestGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_AAITestGameMode_Statics::Class_MetaDataParams), Z_Construct_UClass_AAITestGameMode_Statics::Class_MetaDataParams)
	};
	UClass* Z_Construct_UClass_AAITestGameMode()
	{
		if (!Z_Registration_Info_UClass_AAITestGameMode.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AAITestGameMode.OuterSingleton, Z_Construct_UClass_AAITestGameMode_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AAITestGameMode.OuterSingleton;
	}
	template<> AITEST_API UClass* StaticClass<AAITestGameMode>()
	{
		return AAITestGameMode::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAITestGameMode);
	AAITestGameMode::~AAITestGameMode() {}
	struct Z_CompiledInDeferFile_FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AAITestGameMode, AAITestGameMode::StaticClass, TEXT("AAITestGameMode"), &Z_Registration_Info_UClass_AAITestGameMode, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AAITestGameMode), 2421350644U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_3260204070(TEXT("/Script/AITest"),
		Z_CompiledInDeferFile_FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Unreal_AITest_AITest_Source_AITest_AITestGameMode_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
