// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "MyBTTask_FindRandomPatrol.generated.h"

/**
 * 
 */
UCLASS()
class AITEST_API UMyBTTask_FindRandomPatrol : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
	
public:
	explicit UMyBTTask_FindRandomPatrol(FObjectInitializer const& ObjectInitializer);
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AI")
	float SearchRadius = 1500.f;
};
