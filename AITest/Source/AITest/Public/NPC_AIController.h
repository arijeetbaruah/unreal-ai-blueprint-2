// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionTypes.h"
#include "NPC_AIController.generated.h"

/**
 * 
 */
UCLASS()
class AITEST_API ANPC_AIController : public AAIController
{
	GENERATED_BODY()
public:
	explicit ANPC_AIController(FObjectInitializer const& ObjectInitializer);

	UFUNCTION()
	void OnTargetDetected(const FActorPerceptionUpdateInfo& updateInfo);

protected:
	virtual void OnPossess(APawn* InPawn) override;
	class UAISenseConfig_Sight* SightConfig;

	void SetupPerceptionSystem();

};
