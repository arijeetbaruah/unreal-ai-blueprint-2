//  Fill out your copyright notice in the Description page of Project Settings.


#include "MyBTTask_FindRandomPatrol.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "NavigationSystem.h"
#include "NPC_AIController.h"

UMyBTTask_FindRandomPatrol::UMyBTTask_FindRandomPatrol(FObjectInitializer const& ObjectInitializer)
{
	NodeName = "Find Random Location in NavMesh";
}

EBTNodeResult::Type UMyBTTask_FindRandomPatrol::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (auto* const cont = Cast<ANPC_AIController>(OwnerComp.GetAIOwner()))
	{
		if (auto* const npc = cont->GetPawn())
		{
			auto const origin = npc->GetActorLocation();

			if (auto* const navSys = UNavigationSystemV1::GetCurrent(GetWorld()))
			{
				FNavLocation location;
				if (navSys->GetRandomPointInNavigableRadius(origin, SearchRadius, location))
				{
					OwnerComp.GetBlackboardComponent()->SetValueAsVector(GetSelectedBlackboardKey(), location.Location);
				}

				FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
				return EBTNodeResult::Succeeded;
			}
		}
	}

	return EBTNodeResult::Failed;
}
